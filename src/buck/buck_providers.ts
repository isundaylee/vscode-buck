import * as vscode from "vscode";



interface BuckItem {
    name: string;
}



export class BuckTreeItem extends vscode.TreeItem {
    constructor(label: string, collapsibleState?: vscode.TreeItemCollapsibleState) {
        super(label, collapsibleState);
    }

    get tooltip(): string {
        return "Tool tipperz";
    }

    get description(): string {
        return "rocks!";
    }

    // iconPath = {
    // 	light: path.join(__filename, '..', '..', 'resources', 'light', 'dependency.svg'),
    // 	dark: path.join(__filename, '..', '..', 'resources', 'dark', 'dependency.svg')
    // };

    contextValue = "dependency";
}

const foobar: BuckItem[] = [
    { name: "rainer" },
    { name: "nobrainer" }
];


export class BuckTreeDataProvider implements vscode.TreeDataProvider<BuckItem> {
    onDidChangeTreeData?: vscode.Event<BuckItem | null | undefined> | undefined;

    getTreeItem(element: BuckItem): vscode.TreeItem | Thenable<vscode.TreeItem> {
        return {
            label: `boom! my name is ${element.name}`,
            description: "ship the sauce! :)",
            tooltip: "Someone's greater than noone.",

        } as vscode.TreeItem;
    }
    // onDidChangeTreeData?: vscode.Event<BuckItem | null | undefined> | undefined;    getTreeItem(element: BuckItem): vscode.TreeItem | Thenable<vscode.TreeItem> {
    //     //throw new Error("Method not implemented.");
    // }

    getChildren(element?: BuckItem | undefined): vscode.ProviderResult<BuckItem[]> {
        if (!element) {
            return foobar;
        }

        return [];
    }



}