// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import { Buck } from './buck/buck';
import * as buckConfig from './buck/buck_config';
import { outputChannel } from './buck/buck_logger';
import { BuckProject } from './buck/buck_project';
import { BuckTreeDataProvider } from './buck/buck_providers';


let buckconfigWatcher: vscode.FileSystemWatcher;
let buckFileWatcher: vscode.FileSystemWatcher;


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    // Attempt to restore previous session or load project from workspace.
    loadProject().then((project: BuckProject | null) => {
        const ext = vscode.extensions.getExtension('onqel.vscode-buck');

        // Print welcome message to the screen.
        outputChannel.appendLine(`Welcome to vscode-buck ${
            ext ? 'version ' + ext.packageJSON.version : ''}`);

        if (project === null) {
            outputChannel.appendLine(
                'Workspace does not contain any .buckconfig files!');
            return;
        }

        // TODO: Implement project view
        // const provider = new BuckTreeDataProvider();
        // const foobar123 = vscode.window.createTreeView('buckProjects', {
        // 	treeDataProvider: provider,
        // 	showCollapseAll: true
        // });
        vscode.commands.executeCommand('setContext', 'buckProjectLoaded', true);


        initFileWatchers(context);
        registerVSCodeCommands(project, context);
    });
}

// this method is called when your extension is deactivated
export function deactivate() { }

async function loadProject(): Promise<BuckProject | null> {
    // Find .buckconfig files
    const buckConfigs = await vscode.workspace.findFiles('**/*.buckconfig');

    if (buckConfigs.length > 0) {
        outputChannel.appendLine(
            'Workspace contains the following Buck project files:');
        for (const bc of buckConfigs) {
            outputChannel.appendLine(`  ${bc.path}`);
        }

        // Attempt to load state from previous session.
        const state = buckConfig.getWorkspaceState();

        if (state && buckConfig.shouldUseWorkspaceState()) {
            const p = new BuckProject();
            await p.init(state.projectPath);
            return p;
        }

        // Create a fresh project from root .buckconfig file
        const p = new BuckProject();
        await p.init(buckConfigs[0].fsPath);
        return p;
    }

    return null;
}

function initFileWatchers(context: vscode.ExtensionContext) {
    buckconfigWatcher = vscode.workspace.createFileSystemWatcher(
        '**/*.{buckconfig, buckconfig.*}');
    buckFileWatcher = vscode.workspace.createFileSystemWatcher('**/BUCK');

    // TODO: Watch Buck files
    // context.subscriptions.push(buckconfigWatcher);
    // context.subscriptions.push(buckFileWatcher);

    // Watch for changes in BUCK files
    buckFileWatcher.onDidChange((e) => {
        console.log(`${e.fsPath} did change..`);
    });
    buckFileWatcher.onDidDelete((e) => {
        console.log(`${e} was deleted..`);
        // TODO: Rescan targets
    });

    // Watch for changes in .buckconfig files
    buckconfigWatcher.onDidChange((e) => {
        console.log(`${e} did change`);
    });
}

function registerVSCodeCommands(
    project: BuckProject, context: vscode.ExtensionContext): void {
    const add = (x: vscode.Disposable) => context.subscriptions.push(x);

    // Shows a drop-down list of user-selectable buck targets.
    const showTargets = (x: ((value: string | undefined) => unknown) | undefined) => {
        let activeTarget: string | undefined;

        if (project) {
            const target = project.getCurrentTarget();
            if (target) {
                activeTarget = target.name;
            }
        }
        vscode.window
            .showQuickPick(
                project.targets.map(x => x.name),
                { 'ignoreFocusOut': true, 'placeHolder': activeTarget })
            .then(x);
    };

    // Refresh / Re-init project
    add(vscode.commands.registerCommand('buck.refresh', () => {
        project.init();
    }));

    // Select Buck Project File
    add(vscode.commands.registerCommand('buck.selectProject', () => {
        vscode.workspace.findFiles('**/.buckconfig')
            .then((projects: vscode.Uri[]) => {
                const paths = projects.map(x => x.fsPath);

                vscode.window.showQuickPick(paths).then(selected => {
                    if (selected) {
                        project.init(selected);
                    }
                });
            });
    }));

    // Select Target
    add(vscode.commands.registerCommand('buck.selectTarget', () => {
        showTargets((selectedTarget: string | undefined) => {
            if (selectedTarget) {
                if (project.setCurrentTarget(selectedTarget)) {
                    console.log(`Buck: set active target to ${selectedTarget}`);
                } else {
                    console.log(`Buck: failed to set active target to ${selectedTarget}`);
                }
            }
        });
    }));

    // Buck Build - Selection
    add(vscode.commands.registerCommand('buck.build', () => {
        showTargets((selectedTarget: string | undefined) => {
            if (selectedTarget) {
                const target = project.getTarget(selectedTarget);
                if (target) {
                    target.build();
                }
            }
        });
    }));

    // Buck Build - Active Target
    add(vscode.commands.registerCommand('buck.buildTarget', () => {
        const target = project.getCurrentTarget();
        if (target) {
            target.build();
        }
    }));

    // Buck Run Target
    add(vscode.commands.registerCommand('buck.runTarget', () => {
        const target = project.getCurrentTarget();
        if (target) {
            target.run();
        }
    }));

    // Generate compilation database
    add(vscode.commands.registerCommand(
        'buck.generateCompilationDatabase', () => {
            const target = project.getCurrentTarget();

            if (target) {
                vscode.window.withProgress(
                    {
                        location: vscode.ProgressLocation.Notification,
                        title: 'Buck - Generating compile_commands.json',
                        cancellable: true
                    },
                    (progress: vscode.Progress<{
                        message?: string | undefined;
                        increment?: number | undefined;
                    }>,
                        token: vscode.CancellationToken) => {
                        return target.generateCompilationDatabase(progress, token);
                    });
            }
        }));


    // Buck Clean
    add(vscode.commands.registerCommand('buck.clean', () => {
        Buck({ cwd: project.currentProjectRoot(), cmd: 'clean' });
    }));

    // Buck Audit
    add(vscode.commands.registerCommand('buck.audit', () => {
        vscode.window.showQuickPick(['input', 'cell', 'config', 'flavors'])
            .then(x => {
                const t = project.getCurrentTarget();
                let target = t ? t.name : '';
                let needTarget = false;
                switch (x) {
                    case 'input':
                        needTarget = true;
                        break;
                    case 'cell':
                        break;
                    case 'config':
                        break;
                    case 'flavors':
                        needTarget = true;
                        break;
                    case undefined:
                        return;
                }

                Buck({
                    cwd: project.currentProjectRoot(),
                    cmd: `audit ${x} ${needTarget ? target : ''}`
                }).then(_ => {
                    outputChannel.show();
                });
            });
    }));

    // Buck Query
    add(vscode.commands.registerCommand('buck.query', () => {
        vscode.window
            .showInputBox({
                placeHolder: '//...',
                prompt: 'Buck query arguments (relative to active project)',
                ignoreFocusOut: true
            })
            .then(args => {
                if (args) {
                    Buck({
                        cwd: project.currentProjectRoot(),
                        cmd: 'query',
                        preTargetArgs: [args ? args : '']
                    }).then(_ => outputChannel.show());
                }
            });
    }));
}